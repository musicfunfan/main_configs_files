#!/usr/bin/env bash

killall polybar
cd $HOME/.config/polybar/
polybar example --config=./config.ini
