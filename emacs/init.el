;;; package --- Evil; flycheckm; timu-caribbean-theme;

;;; Commentary:

;;This is the init.el file.

;;; code:

;;Enable line number.
(global-display-line-numbers-mode 1)

;;Set font size
(set-face-attribute 'default nil :height 200)

;;Adding MELPA rep.
(require 'package)
(add-to-list 'package-archives'("MELPA Stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)
(package-refresh-contents)

(unless (package-installed-p 'evil)
  (package-install 'evil))

;; Enable Evil
(require 'evil)
(evil-mode 1)

;;Install the flycheck package.
(package-install 'flycheck)

;;Enable the filecheck package.
(global-flycheck-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(goto-chg undo-fu undo-tree terminal-here evil timu-caribbean-theme flycheck)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;Load the timu-caribbeab theme.
(load-theme 'timu-caribbean t)

;;Load electric-pair
(electric-pair-mode t)

(windmove-default-keybindings)

(evil-set-undo-system 'undo-redo)

;;; init.el ends here
