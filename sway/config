#Set mod keys
set $mod1 Mod1
set $mod Mod4
set $mod3 control


# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 12


#This is to enable gaps on the window manager
#gaps inner 1
#gaps outer 2

#This is to enable smart gaps.
#smart_gaps on

# set windows borders
default_border pixel 5

default_floating_border pixel 9 
hide_edge_borders smart

# class                 border  backgr. text    indicator child_border
client.focused          #04f4d8 #011570 #00f9c3 #0196a0   #0196a0
client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #333333 #222222 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff


# this is for mpv to be floating 
#for_window [class="mpv"] floating enable,sticky enable

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec kitty

# kill focused window
bindsym $mod1+F4 kill

#change the wallpeper
bindsym $mod+w exec $HOME/.config/scripts/SelectWallpeper.sh
#enable loopback sound  
bindsym $mod1+f exec  pacmd load-module module-loopback latency_msec=0

bindsym $mod+p exec ~/.config/scripts/power.sh &
bindsym $mod1+n exec ~/.config/scripts/sites.sh &

# start dmenu (a program launcher)
#bindsym $mod1+space exec dmenu_run -nf "#00A5FF" -sf '#adff2f' -nb '#000000'

#start rofi (a another program launcher)
bindsym $mod1+space exec rofi -show drun -theme $HOME/.config/rofi/decu_edited.rasi

#start rofi to select emoji
bindsym $mod1+j exec rofi -show emoji -modi emoji -theme $HOME/.config/rofi/old_config.rasi

#Binds to mount and umount stuff
bindsym $mod+m exec /home/musicfan/.config/scripts/mount_stuff.sh $

#open the firefox 
bindsym $mod1+w exec firefox -P Normal 

#open pavucontrol
bindsym $mod+a exec passmenu 

#stiky mode 
bindsym $mod+s sticky toggle


#This is to change the bar
bindsym $mod+c exec ~/.config/scripts/changeBar.sh
bindsym $mod+shift+c exec ~/.config/scripts/changeBar.sh -2

#take screenshot
bindsym Print exec ~/.config/scripts/screenshot.sh

#Media scripts keybind
bindsym control+shift+w exec ~/.config/scripts/media.sh $ 

#Show the calendat 
bindsym mod1+c exec gsimplecal &

#Change the keyboard layout
#bindsym $mod1+l exec ~/.config/scripts/language.sh

#lock the i3
bindsym $mod+shift+l exec i3lock --image=/home/musicfan/icons/wallpeper.png

 
#audio 
bindsym XF86AudioMute exec ~/.config/scripts/volumeMeter.sh -m & 
bindsym XF86AudioLowerVolume exec ~/.config/scripts/volumeMeter.sh -d &
bindsym XF86AudioRaiseVolume exec ~/.config/scripts/volumeMeter.sh -u $

# Media player controls
bindsym XF86AudioPlay exec ~/.config/scripts/mediaNotification.sh -p &
bindsym XF86AudioPause exec ~/.config/scripts/mediaNotification.sh -p &
bindsym XF86AudioNext exec ~/.config/scripts/mediaNotification.sh -n &
bindsym XF86AudioPrev exec ~/.config/scripts/mediaNotification.sh -b &
bindsym XF86AudioStop exec ~/.config/scripts/mediaNotification.sh -t &

#control the brightness control 
#brightness up 
#bindsym XF86MonBrightnessUp exec xbacklight -inc 20
#brightness down
#bindsym XF86MonBrightnessDown exec xbacklight -dec 20

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+d move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+a move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h 

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod1+5 fullscreen toggle

# change container  (stacked, tabbed, toggle split)
bindsym $mod1+3 layout stacking
bindsym $mod1+4 layout tabbed
bindsym $mod1+6 layout toggle split

# toggle tiling / floating
bindsym $mod1+t floating toggle

#floating windows
for_window [class="Nsxiv"] floating enable,resize set 3200 2000


# change focus between tiling / floating windows
bindsym $mod1+b focus mode_toggle

# focus the parent container
bindsym $mod1+a focus parent

#scratch pad
bindsym $mod+Shift+minus move scratchpad

bindsym $mod+minus scratchpad show

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "🖖"
set $ws2 "💿"
set $ws3 "🎦"
set $ws4 "🔎"
set $ws5 "🛠️"
set $ws6 "🔮"
set $ws7 "💼"
set $ws8 "🔴"
set $ws9 "🔞"
set $ws10 "🎮"

# switch to workspace
bindsym $mod3+F1 workspace $ws1
bindsym $mod3+F2 workspace $ws2
bindsym $mod3+F3 workspace $ws3
bindsym $mod3+F4 workspace $ws4
bindsym $mod3+F5 workspace $ws5
bindsym $mod3+F6 workspace $ws6
bindsym $mod3+F7 workspace $ws7
bindsym $mod3+F8 workspace $ws8
bindsym $mod3+F9 workspace $ws9
bindsym $mod3+F10 workspace $ws10

# move focused container to workspace
bindsym $mod3+Shift+F1 move container to workspace $ws1
bindsym $mod3+Shift+F2 move container to workspace $ws2
bindsym $mod3+Shift+F3 move container to workspace $ws3
bindsym $mod3+Shift+F4 move container to workspace $ws4
bindsym $mod3+Shift+F5 move container to workspace $ws5
bindsym $mod3+Shift+F6 move container to workspace $ws6
bindsym $mod3+Shift+F7 move container to workspace $ws7
bindsym $mod3+Shift+F8 move container to workspace $ws8
bindsym $mod3+Shift+F9 move container to workspace $ws9
bindsym $mod3+Shift+F10 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+b reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"
#Lock the screen
bindsym $mod1+l exec betterlockscreen --lock

bindsym $mod+u exec dunstctl set-paused toggle

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym a resize shrink width 10 px or 10 ppt
        bindsym s resize grow height 10 px or 10 ppt
        bindsym q resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt
							
        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"

}

bindsym $mod+r mode "resize"

# Switch between US (international) and Greek.
input * {
        xkb_layout "us","gr"
	xkb_options grp:alt_shift_toggle
}
include /etc/sway/config.d/*

exec_always waybar &

#set the wallpepe
exec_always swaybg -m fit -i  /home/$USER/icons/wallpapers/0071.jpg &
#exec picom --config ~/.config/picom/picom.conf &

#start polkit
exec gnome-keyring-daemon &
exec lxpolkit &

#start the notification system
exec dunst & 

exec radeontop -d gpu.txt -l 0 > /dev/null &

#start openrgb
exec openrgb -p profile1.orp --startminimized & 

#start the network-manager applet on system tray 
exec nm-applet &

#Enabling the pulseaudio server (on pipewire) load the tcp module
exec_always pactl load-module module-native-protocol-tcp auth-anonymous=true

#Start playerctl daemon
exec_always playerctld daemon &

#This is to change the temprature of the screen
exec_always wlsunset &

#Add Variables for OBS and other to work.
exec_always dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK
exec_always dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway

#Start the Xresources
exec_always xrdb -load /home/musicfan/.Xresources &

#Open the corectrl to control gpu 
exec_always corectrl -m main --minimize-systray
