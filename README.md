# What we have here ?

This is my personal configuration files for Linux. This is a collection of configuration files 
i use in my computer (archlinux). A lot of all that files has be taken from the internet i will try to add 
the authors of all of that or the site i copy stuff from.

# Why this exist ?

This is just for backup and share this files with my friends and the world as an idea to how can you set up a Linux machine.

# How can i use this ?

You can clone the repo take any of the files and replace (**keep backups**) your configs with mine and check if it working or not. If not well... Or you try to fix it or you use your old config (**from the backups**). You can if you want to open an issue but this is files for my settings so if it works for me i am not gonna fix any problem you have. This is your job. You can create a pull request if its something i like and i need to use, i will merge it if not i will not merge it.

# What you can find here ?

* my Ranger file manager settings 
* my Mpv config file
* my Mpv collection of lus scripts taken off the internet
* my .bashrc file
* my Neofetch config 
* my i3wm configs 
* my Picom config file
* my Polybar config file
* my Alacritty config file
* my Dunst config file
* my Vim config file
* my Topgrade configuration
* my Rofi configs (Most configs its not mine. I just upload them here in order to have what i use in one place. If you are open the ``*.rasi`` file should have the copyrights/gredits. If not contact me.)
* my Sway configs
* my Hyprland configs
* my Sddm configs
* my Doom emacs configs
* my kitty configs 

And many more. 


## mpv scripts origins.

The mpv scripts i use on this repo is not mine. I have taken from the githubs links bellow.

* https://github.com/po5/thumbfast (thumbfast script)
* https://github.com/jonniek/mpv-playlistmanager (mpv-playlistmanager)
* https://github.com/occivink/mpv-gallery-view/tree/master (mpv-gallery-view)


# Disclaimer 

This files is working for my system. I do not know what they will do in your system, so if you do not know what you are doing **do not use them**. I am not responsible for what happens on your systems with my configs files. **I am not a professional** i am just a kid i like computers. 

# How to be safe ?

I recommend **keep backups** and snapshots of your system. Just to be in the safe side.

# I may have many mistakes

I am not good with writing so i may have typos and syntax errors in my text. Sorry about that.

# screenshot 

This how my system looks like:

![IMAGE_DESCRIPTION](screenshot_to_show_my_desktop.png)
