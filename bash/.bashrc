# _ __ ___  _   _ ___(_) ___ / _|_   _ _ __  
#| '_ ` _ \| | | / __| |/ __| |_| | | | '_ \ 
#| | | | | | |_| \__ \ | (__|  _| |_| | | | |
#|_| |_| |_|\__,_|___/_|\___|_|  \__,_|_| |_|

#this is the musicfuns .bashrch file !!!


# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
#alias to display drivers in use 
alias drivers='lspci -v'
#alia for watch the cpu freq
alias cpufreq='watch grep \"cpu MHz\" /proc/cpuinfo'
#alias to see temps via terminal
alias cputemps='watch sensors'
#alias for go to hdd fast
alias hdd=' cd /media/$USER/hdd/'
#alias to check disk space
alias diskspace='watch df -h'
#alias for help with my commands
alias myhelp='cat /media/$USER/hdd/linux_stuf/linux_repo/memory/linux_helps_boost ' 
#alias to go to the music folder
alias music='cd /media/$USER/hdd/Μουσική/' 
#alias to go to videos
alias video='cd /media/$USER/hdd/video/'
#alias to go to documents
alias doc='cd /media/$USER/hdd/doc/'
#alias to go to icons
alias icon='cd /media/$USER/hdd/Εικόνες/'
#alias to go to iso folder
alias iso='/media/musicfan/hdd/isos'
#alias for the size of the files 
alias size='ncdu '                      
#alias for reaper daw
alias reaper='cd /home/$USER/reaper_linux_x86_64; ./install-reaper.sh'
#alias for shotcuthelp
alias helpkey='cat /media/$USER/hdd/linux_stuf/linux_repo/memory/shortcuts'
#alias to go to atei folder
alias tei=' cd /media/$USER/hdd/doc/A.T.E.I'
#alias for me to remeber my alias
alias myalias='cat /media/$USER/hdd/linux_stuf/linux_repo/memory/myaliasforbash'
#alias for mount the philips 
alias philipsmount='sudo mount -t ntfs -o defaults /dev/sde1 /media/$USER/philips/'
#alias to umount the philips
alias philipsumount='sudo umount /media/$USER/philips'
#alias to mount sandisk
alias sandiskmount='sudo mount -t ext4 -o defaults /dev/sdd1 /media/$USER/sandisk/'
#alias to umount the sandisk
alias  sandiskumount='sudo umount /media/$USER/sandisk/'
#alias to see the bios version and other
alias bios='sudo dmidecode | less'
#alias to to see help my file for pakage
alias helppakage='cat /media/$USER/hdd/linux_stuf/pakage_drivers'
#alias to copy the path of a file to clipboard
alias copyfilepath='read in; readlink -f $in |xclip -sel clip'
#alias to mount the hdd
alias mhdd='sudo mount /dev/sdd2 /media/$USER/hdd'
#alias for the pwd copy path
alias pwdc='pwd | xclip -selection clipboard'
#alias to anycommand to copy out put 
alias copy='xclip -selection clipboard'
#alias to lsd command
alias la='lsd -lha'
#alias for refreshing the update counter
alias updates='sudo timeshift --create; yay -Suy; ~/.config/dash2.sh; sudo timeshift --delete'
#alias to go to the ytplayer fast
alias ergasia='cd /media/$USER/hdd/doc/YoutubePlayer/Ytplayer/Ytplayer/ergasia_hmu'
#this is to mount onedrive
alias monedrive='rclone --vfs-cache-mode writes mount OneDrivePersonal: ~/OneDrivePersonal &'
#alis for the vms ssd to mount 
alias mvms='sudo mount /dev/sdc2 /var/lib/libvirt/images; sudo virsh net-start default;'
#alias for the camera using mpv
alias camera='mpv av://v4l2:/dev/video0 --profile=low-latency --untimed'
#this is alias to show the weather 
alias weather='curl wttr.in'
#this is alias to remove orphande packages 
alias cleanpg='sudo pacman -Rs $(pacman -Qtdq)'
#alias this is an alias for the mount laptop
alias mlaptop='sudo sshfs -o allow_other musicfunl@192.168.1.9:/home/musicfunl/main /media/$USER/laptop/'
#this is alias to go to the laptop location 
alias laptop='cd /media/$USER/laptop'
#alias this is an alias for the mount q6600
alias mq6600='sudo sshfs -o allow_other musicfun@192.168.1.29:/home/musicfun/main /media/$USER/q6600/'
#this is alias to go to the q6600 location 
alias q6600='cd /media/$USER/q6600'
#alias for script folder
alias scripts='cd /home/$USER/.config/scripts'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
#alias to go to the script direcotory
alias scripts='cd ~/.config/scripts'
#alias to  ssh to q6600
alias sshq6600='ssh musicfun@192.168.1.29'
#alias to ssh to laptop
alias sshlaptop='ssh musicfunl@192.168.1.9'
#alias to sync the doc files with the laptop  doc files 
alias syncdoclaptop='rsync -avh --info=progress2 /media/$USER/hdd/doc musicfunl@192.168.1.9:/home/musicfunl/main/doc --delete'
#alias this is to create subdirectorys with just typing mkdir 
alias mkdir='mkdir -pv'
#alias to go to the the git_repo
alias gitrepo='cd /home/$USER/gitrepo'
#alias to mount kingston flash drive
alias mkingston='sudo mount /dev/sde1 /media/$USER/kingston'
#alias to go to kingston usb flash drive
alias kingston='cd /media/$USER/kingston'
#alias to lsblk
alias lsblk='lsblk -o name,label,size,mountpoint,model,UUID'
#alias to du
alias du='du -h | sort -hr '
#alias to extract tar.gz files
alias extract='tar -xf'
#alias for 
#alias rofi='rofi -theme /home/$USER/.config/rofi/second_theme/dmenu_edited.rasi'
#alias to kill bottles.
alias killbottle='flatpak kill com.usebottles.bottles'


#Make vim the default editor
export EDITOR=vim

#Ad .local/bin to the path
export PATH=/home/$USER/.local/bin:$PATH

#Ad doom emacs to the path
export PATH=/home/$USER/.config/emacs/bin:$PATH

#Set the starship prompt
eval "$(starship init bash)"

echo "welcome $USER💗"
date
echo "---------------------------------------------------------------------------------------------------------------------------"
fastfetch
echo "---------------------------------------------------------------------------------------------------------------------------"

PATH=/home/msuicfun/.cargo/bin:$PATH
PATH=/home/musicfan/.cargo/bin:$PATH
