# Here is the dependencies for the .bashrc file

* xclip
* timeshift 
* neofetch 
* lm_senors
* lsd 
* rclone
* curl
* sshfs 
* ssh
* ncdu
* notify-send